const restify = require('restify');
const exec = require('child_process').exec;

const server = restify.createServer({
  name: 'localhost',
  version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.get('/exec', function (req, res, next) {
  exec('sh script.sh', (error, stdout, stderr) => {
    console.log(`${stderr}`, 'err');
    if (error) {
      console.log(`exec error: ${error}`);
    }
    res.send(`Result: ${stdout}`);
    return next();
  });

});

server.listen(8000, function () {
  console.log('%s listening at %s', server.name, server.url);
});
