var firebase = require('firebase-admin');
var request = require('request');
var _ = require('lodash')

var serviceAccount = require("./nomadapp-key.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://nomadapp-3f091.firebaseio.com/"
});

ref = firebase.database().ref();
var FCM = ref.child('FCM')
var fcmData = null
FCM.on('value', fcm => {
  fcmData = fcm.val()
})
function watchPostNotification() {
  var PostNotification = ref.child('NodePostNotification');
  PostNotification.on('child_added', function(requestSnapshot) {
    var child = requestSnapshot.val();
    if (child && child.Small) {
      _.forEach(child.Small, notification => {
        if (notification && notification.uid == fcmData.uid) {
          notifyUser(
            fcmData
          );
        }
      })
    }
  }, function(error) {
    console.error(error);
  });
};

function notifyUser(fcm) {
  var registrationToken = fcm.devicetoken;
  var payload = {
    notification: {
      title: "Notification Title",
      body: "Notification from faraz"
    }
  };
  firebase.messaging().sendToDevice(registrationToken, payload)
  .then(function(response) {
    // See the MessagingDevicesResponse reference documentation for
    // the contents of response.
    console.log("Successfully sent message:", JSON.stringify(response));
  })
  .catch(function(error) {
    console.log("Error sending message:", error);
  });
}

watchPostNotification();
