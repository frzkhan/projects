function processData(input) {
    var lines = input.split('\n');
    var dimension = parseInt(lines[0]);
    var grid = [];
    for(var i = 1; i <= dimension; ++i)
    {
        grid.push(lines[i]);
    }
    displayPathtoPrincess(dimension, grid);
}
function timesLog(number, message) {
    for(var i = 1; i <= number; i++) {
       console.log(message)
    }
}
function displayPathtoPrincess(dimension, grid)
{
    var botRow = null
    var botColumn = null
    var pRow = null
    var pColumn = null
    for(var i =0; i < dimension; ++i){
        if(/m/.test(grid[i])) {
            botRow = i
            botColumn = grid[i].split("").indexOf('m')
        }
        if(/p/.test(grid[i])) {
            pRow = i
            pColumn = grid[i].split("").indexOf('p')
        }
    }

    if(botRow < pRow) {
        timesLog((pRow - botRow), 'DOWN')
    }
    if(botRow > pRow) {
        timesLog((botRow - pRow), 'UP')
    }
    if(botColumn < pColumn) {
        timesLog((pColumn - botColumn), 'RIGHT')
    }
    if(botColumn > pColumn) {
        timesLog((botColumn - pColumn), 'LEFT')
    }
}
processData(`5
  ----------
  --------m-
  p---------`)
// process.stdin.resume();
// process.stdin.setEncoding("ascii");
// _input = "";
//
// process.stdin.on("data", function (input) {
//     _input += input;
// });
//
// process.stdin.on("end", function () {
//    processData(_input);
// });
