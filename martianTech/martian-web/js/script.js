$(document).on('click', '.panel-heading span.icon_minim', function (e) {
  var $this = $(this);
  if (!$this.hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideUp();
    $this.addClass('panel-collapsed');
    $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
  } else {
    $this.parents('.panel').find('.panel-body').slideDown();
    $this.removeClass('panel-collapsed');
    $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
  var $this = $(this);
  if ($('#minim_chat_window').hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideDown();
    $('#minim_chat_window').removeClass('panel-collapsed');
    $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('click', '#new_chat', function (e) {
  var size = $( ".chat-window:last-child" ).css("margin-left");
  size_total = parseInt(size) + 400;
  alert(size_total);
  var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
  clone.css("margin-left", size_total);
});
$(document).on('click', '.icon_close', function (e) {
  //$(this).parent().parent().parent().parent().remove();
  $( "#chat_window_1" ).remove();
});

angular.module('app.chat', ['luegg.directives'])
.config(function($locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
})
.controller('chatBot', function ($scope, $http, $location, $anchorScroll) {
  var sessionId = new Date().getTime()
  $scope.messages = []

  $scope.send = function () {
    var msg = $scope.message
    if(!msg) return
    if($scope.typing) return
    $scope.typing = true
    $scope.messages.push({
      id: new Date().getTime(),
      speech: msg,
      time: new Date(),
      name: 'me'
    })
    $scope.message = ''
    $http({
      method: 'GET',
      url: 'https://martiantechnologies.herokuapp.com/?m=' + msg + '&s=' + sessionId
    })
    .then(function (response) {
      $scope.typing = false
      var data = response.data
      handleAction(data.result.action)
      $scope.messages.push({
        id: new Date().getTime(),
        speech: data.result.fulfillment.speech,
        name: 'bot',
        time: new Date(data.timestamp)
      })
    })
  }
  function handleAction (action) {
    switch(action) {
      case 'contact':
      var newHash = 'contact';
      if ($location.hash() !== newHash) {
        $location.hash('contact');
      } else {
        $anchorScroll();
      }
      break;
    }
  }
})
.directive('onEnter',function() {
  var linkFn = function(scope,element,attrs) {
    element.bind("keypress", function(event) {
      if(event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.onEnter);
        });
        event.preventDefault();
      }
    });
  };

  return {
    link:linkFn
  };
});
