const tr = require('tor-request');
const json2csv = require('json2csv');
const cheerio = require('cheerio')
const P = require('bluebird')
const fs = require('fs')
const ProgressBar = require('progress');
const argv = require('minimist')(process.argv.slice(2));

const start = argv.start
const end = argv.end
const ips = [
  ['61.7.181.41', '52335'],
  ['186.227.8.21', '52335'],
  ['203.58.117.34', '80'],
  ['210.212.73.61', '80'],
  ['203.74.4.1', '80']
]
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
const bar = new ProgressBar('  scraping [:bar] :rate/bps :percent :etas', {
    complete: '=',
    incomplete: ' ',
    width: 20,
    total: end-start
  });
let url = "http://support.dudegadgets.com/DGCustomerService/index.php?view=issueOrderSeachResult&orderName=DG"
function tick() {
  bar.tick();
  if (bar.complete) {
    console.log('\ncomplete\n');
  }
}
function sendRequest(orderName, delay) {
  //return P.delay(delay)
  //.then(() => {
  //  const randomIp = getRandomInt(0,4)
  //  tr.setTorAddress(ips[randomIp][0], ips[randomIp][1]);
    return new Promise((resolve, reject) => {
      tr.request(`${url}${orderName}`, (err, res, body) => {
        if(err) {
          tick()
          reject(err)
          return
        }
        const $ = cheerio.load(body)
        const order = {
          name: orderName,
          created: $('#detailForm input[name=orderID]').val(),
          customerName: $('#detailForm input[name=customerName]').val(),
          emailAddress: $('#detailForm input[name=emailAddress]').val(),
          orderStatus: $('#detailForm textarea[name=orderStatus]').val(),
          shippingOption: $('#detailForm span[for=shippingOption]').html(),
          shippingProvider: $('#detailForm input[name=shippingProvider]').val()
        }
        tick()
        resolve(order)
      })
    })
    .then(data => {
      try {
        var result = json2csv({ data: [data], hasCSVColumnTitle: false });
        console.log(result);
        fs.appendFile('orders.csv', result, (err) => {
          if(err) {
            console.log(err);
          }
        })
      } catch (err) {
        console.error(err);
      }
    })
  //})
  .catch(error => {
    console.log(error);
  })
}
const requests = []
let delay = 0
for (let i = end; i >= start; i --) {
  delay+=100
  requests.push(i)
}

P.each(requests, i => {
  return sendRequest(i)
})
// .then(results => {
//   console.log(results);
//   try {
//     var result = json2csv({ data: results });
//     fs.writeFile('orders.csv', result, (err) => {
//       if(err) {
//         console.log(err);
//       }
//       console.log('Finished!');
//     })
//   } catch (err) {
//     console.error(err);
//   }
// })
