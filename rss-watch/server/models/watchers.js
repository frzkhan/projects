'use strict';
const Watcher = require('rss-watcher');
const watchersList = {}

module.exports = function(Watchers) {

  Watchers.observe('after save', (ctx, next) => {
    const data = ctx.instance

    if (data && data.active && data.link) {
      const watcher = new Watcher(data.link);

      watcher.on('new article', article => {
        console.log('new article: ',data.id,  article.title);
      })
      watcher.on('stop', () => {
        delete watchersList[data.id]
        console.log('stopped', data.id);
      })
      watcher.run((err, articles) => {
        if (err) {
          console.error(err);
        }
        return console.log('articles: ', articles.length);
      });
      watchersList[data.id] = watcher
    }

    if (data && !data.active && watchersList[data.id]) {
      watchersList[data.id].stop()
    }
    console.log(watchersList);
    next();
  })
};
