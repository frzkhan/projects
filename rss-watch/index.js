var Watcher, feed, watcher;

Watcher = require('rss-watcher');

feed = 'https://newyork.craigslist.org/search/cta?format=rss';

watcher = new Watcher(feed);

watcher.on('new article', function(article) {
  return console.log(article);
});
watcher.on('stop', function() {
  return console.log('stopped');
});

console.log(JSON.stringify(watcher));
watcher.run(function(err, articles) {
  if (err) {
    console.error(err);
  }
  return console.log(articles);
});
