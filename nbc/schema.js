var schema = {
  name: String,
  email: String,
  phoneNumber: String,
  productionName: String,
  location: String,
  orderDetails: String,
  equipmentRequested: null,
  department: null,
  deliveryDate: Date,
  notes: String
}
