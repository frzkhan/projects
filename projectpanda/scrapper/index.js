//solve problem of downloading mass amount of files and possible EventEmitter memory leak error
require('events').EventEmitter.defaultMaxListeners = 0

const request = require('request-promise'),
    cheerio = require('cheerio'),
    steam = require('./steam'),
    support = require('./support'),
    database = require('./database'),
    domain = 'https://www.g2a.com',
    DONE_FLAG = "DONE",
    rows = 10,
    PQueue = require('p-queue'),
    queue = new PQueue({ concurrency: 10 })
let consoleAndPlatform = [],
    titleFilters = [],
    gameTable = [],
    gameEditionTable = [],
    gameLanguages = [],
    startMoment,
    currencyRates = null,
    projectPath = support.getProjectPath(),
    startFrom = 0,
    totalRecords = 5000,
    doneRecords = 0,
    parseAllRecords = false

steam.init().then(() => {
    return databaseInit()
}).then(() => {
    return request(domain + '/currency-rates.js')
}).then(rates => {
    startMoment = support.getCurrentMoment()
    currencyRates = rates.replace('window.rates = ', '')
    currencyRates = JSON.parse(currencyRates.replace(';', ''))
}).then(() => {
    parserRequests()
}).catch(error => {
    console.log(new Error(error))
})

const parserRequests = function () {
    if (startFrom <= totalRecords) {
        setTimeout(function () {
            getParserOffers(startFrom)
            startFrom += rows
            process.nextTick(() => {
              parserRequests()
            })
        }, 1000)
    }
}

const getParserOffers = function (start) {
    request({
        uri: domain + '/lucene/search/filter?minPrice=0.00&maxPrice=639.63&stock=all&cat=0&sortOrder=added+desc&start=' + start + '&rows=' + rows + '&steam_app_id=&steam_category=&steam_prod_type=&includeOutOfStock=&includeFreeGames=false',
        json: true
    }).then(response => {
        if (parseAllRecords) {
            totalRecords = response.numFound
            parseAllRecords = false
        }
        const promises = response.docs.map(doc => getOffersData(doc))
        return Promise.all(promises)
    }).then(gamesList => {
        doneRecords += rows
        console.log('Done records: ', doneRecords)
        if (doneRecords >= totalRecords) {
            Promise.resolve(gameTable.sort(support.dynamicSort("title"))).then(() => {
                support.getMomentElapsedTime(startMoment)
                process.exit()
            })
        }
    }).catch(error => {
        console.error('ERROR: Server did not respond')
    })
}

const databaseInit = function () {
    return database.consoleAndPlatform.all().then(elements => {
        elements.forEach(function (element) {
            consoleAndPlatform.push(element.dataValues)
        })
    }).then(() => {
        return database.titleFilters.all().then(elements => {
            elements.forEach(function (element) {
                titleFilters.push(element.dataValues)
            })
        })
    }).then(() => {
        return database.gameLanguages.all().then(elements => {
            elements.forEach(function (element) {
                gameLanguages.push(element.dataValues)
            })
        })
    })
}

const nameFilter = function (name) {
    return replaceTitle(name).then(result => {
        return replaceRegion(result)
    }).then(result => {
        return result.replace(/ +(?= )/g, '').trim()
    }).catch(error => {
        console.log(error)
    })
}

const replaceTitle = function (name) {
    return support.returnObjectByPropertyInArray(titleFilters, "filterName", "replaceTitle").then(obj => {
        if (obj) {
            return name.substr(0, 1) + name.substr(1, name.length).replace(new RegExp(obj.regex), '')
        } else {
            throw "No 'replaceTitle' property in title_parsers"
        }
    })
}

const replaceRegion = function (name) {
    return support.returnObjectByPropertyInArray(titleFilters, "filterName", "replaceRegion").then(obj => {
        if (obj) {
            return name.replace(new RegExp(obj.regex, 'g'), '')
        } else {
            throw "No 'replaceRegion' property in title_parsers"
        }
    })
}

const checkEdition = function (name) {
    return support.isStringContains(name, new RegExp(titleFilters[2].regex, 'g'))
}

const getConsoleAndPlatform = function (title) {
    let game = {
        console: null,
        platform: null
    }

    return new Promise(function (resolve) {
        consoleAndPlatform.some(function (arrayItem) {
            let platform = arrayItem.platform
            if (support.isStringContains(title, platform)) {
                game.console = arrayItem.console
                game.platform = arrayItem.name
                return true
            }
        })
        resolve(game);
    })
}

const getPrices = function (id) {
    let mainCurrency = "EUR"

    return request({
        uri: domain + '/marketplace/product/auctions/?id=' + id,
        jar: support.setCookieForUrl({
            key: "currency",
            value: mainCurrency,
            domain: "www.g2a.com",
            httpOnly: true,
            maxAge: 31536000
        }, domain),
        json: true
    }).then(auctionData => {
        let gamePrices = {}

        if (auctionData.lowest_price) {
            gamePrices[mainCurrency] = auctionData.lowest_price
        } else {
            if (auctionData.a) {
                Object.keys(auctionData.a).forEach(offerId => {
                    let minPrice
                    let offer = auctionData.a[offerId]
                    if (!minPrice || offer.ep < minPrice) {
                        minPrice = offer.ep
                    }
                })
                gamePrices[mainCurrency] = minPrice
            } else {
                throw "No auctionData to parse"
            }
        }

        Object.keys(currencyRates).forEach(key => {
            gamePrices[key] = (gamePrices[mainCurrency] * currencyRates[key]).toFixed(2)
        })
        return gamePrices
    })
}

const getDesctiption = function (dom) {
    return dom(".jq-product-description").text()
}

const getGallery = function (dom) {
    let galleryArray = []
    dom('.cw-img-list li').each(function (i, e) {
        var url = dom(this).find('a').attr("href")
        if (!support.isStringContains(url, "youtube"))
            if (support.isImageLandscapeMode(url).then(result => {
                return result
            })) galleryArray.push(url)
    })
    return galleryArray
}

const getRegion = function (dom) {
    return new Promise(function (resolve) {
        dom('.games-info li').each(function (i, e) {
            var label = dom(this).find('.game-info-label').text().trim()
            if (label == 'Region:') {
                resolve(dom(this).find('.game-info-content').text())
            }
        })
    })
}

const getLanguages = function (dom) {
    let languages = []

    dom('.games-info li').each(function (i, e) {
        var label = dom(this).find('.game-info-label').text().trim()
        if (label == 'Language available:') {
            dom(this).find('.game-info-content .flag img').each(function () {
                var languageHash = dom(this).attr('alt')
                gameLanguages.forEach(function (element) {
                    if (element["languageHash"] == languageHash) {
                        languages.push(element["languageName"])
                    }
                })
            })
        }
    })
    return languages
}

const isTitleNotProhibited = function (dom, gameTitle) {
    //checking if game is not application or regex string
    return (!support.elemExist(dom('h4#app-name')) && !support.isStringContains(gameTitle, /(\+|&|SKIN|MAC|Nintendo|Nintedo)/g)) ? true : false
}

const getOffersData = function (doc) {
    var url = domain + doc.slug

    return queue.add(() => {
        return request(url + "?___store=englishus").then(body => {
            const $ = cheerio.load(body)
            //check if game is not from prohibited category
            if (isTitleNotProhibited($, doc.name)) {
                return nameFilter(doc.name).then(gameTitle => {
                    //check if gameTitle isnt in gameTable
                    return support.findObjectPropertyInArray(gameTable, "title", gameTitle).then(result => {
                        if (result) {
                            console.log("id:" + doc.id + " title:" + gameTitle)
                            return DONE_FLAG
                        } else {
                            const game = {
                                id: doc.id,
                                title: gameTitle,
                                url: url,
                            }
                            return getConsoleAndPlatform(doc.name).then(result => {
                                game.console = result.console
                                game.platform = result.platform
                            }).then(() => {
                                return getRegion($).then(region => {
                                    game.region = region
                                }).then(() => {
                                    return getPrices(doc.id)
                                }).then(prices => {
                                    game.prices = prices
                                    game.languages = getLanguages($)
                                })
                            }).then(() => {
                                if (game.platform === "Steam") {
                                    return steam.getSteamInfo(gameTitle)
                                        .then(function (steamInfo) {
                                            if (steamInfo) {
                                                game.steamId = steamInfo.steamId
                                                game.genres = steamInfo.genres
                                            }
                                        })
                                } else {
                                    return DONE_FLAG
                                }
                            }).then(() => {
                                gameTable.push(game)
                                return DONE_FLAG
                            })
                        }
                    })
                })
            }
        }).catch(error => {
            if (error.statusCode !== 404) {
                console.log(new Error(error))
                console.log("Error occured with ", url)
            }
        })
    })
}
