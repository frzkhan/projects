const Sequelize = require('sequelize'),
  sequelize = new Sequelize('postgres://postgres:facit@localhost:5432/project_panda'),
  consoleAndPlatform = sequelize.import(__dirname + "/models/consoleAndPlatform"),
  game = sequelize.import(__dirname + "/models/game"),
  titleFilters = sequelize.import(__dirname + "/models/titleFilters"),
  gameLanguages = sequelize.import(__dirname + "/models/gameLanguages"),
  fs = require('fs')

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
    sequelize.sync({
      force: false // if new properties where added to the tables it will drop the table and recreate it if true
    }).then(() => {
      //you can fill up your database from JSON file
      bulkCreateOrFindFromJSON(titleFilters, __dirname + "/databaseTables/titleFilters.json")
      bulkCreateOrFindFromJSON(consoleAndPlatform, __dirname + "/databaseTables/consoleAndPlatform.json")
      bulkCreateOrFindFromJSON(gameLanguages, __dirname + "/databaseTables/gameLanguages.json")
    })
  }).catch(err => {
    console.error('Unable to connect to the database:', err)
  });

const bulkCreateFromJSON = function (model, jsonPath) {
  fs.readFile(jsonPath, function (err, data) {
    model.bulkCreate(JSON.parse(data))
  })
}

//Insert data if doesn't exist or don't do anything
const bulkCreateOrFindFromJSON = function (model, jsonPath) {
  fs.readFile(jsonPath, function (err, data) {
    if (err) throw err
    else if (data) {
      JSON.parse(data).forEach(function (elem) {
        model.findOrCreate({
          where: elem
        })
      })
    }
  })
}

exports.game = game
exports.consoleAndPlatform = consoleAndPlatform
exports.titleFilters = titleFilters
exports.gameLanguages = gameLanguages
