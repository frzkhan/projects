const tabletojson = require('./tableToJSON'),
    request = require('request-promise'),
    support = require('./support'),
    cheerio = require('cheerio'),
    sleep = require('system-sleep')
    steamAPIURL = "http://api.steampowered.com/ISteamApps/GetAppList/v0001/",
    steamDBURL = "https://steamdb.info"
let steamAppsArr = [],
    steamGameParsed = 0


exports.init = function (init) {
    return request({
        url: steamAPIURL,
        json: true
    })
        .then(function (body) {
            steamAppsArr = body.applist.apps.app
            return steamAppsArr
        }).catch(error => {
            console.log(new Error(error))
        })
};

exports.getSteamInfo = function (gameTitle) {
    let languages = ["english", "german", "polish", "french", "spanish", "russian"]

    return getSteamIDbyTitle(gameTitle).then(steamID => {
        if (steamID) {
            return getSteamInfoFromAPI(steamID)
        } else {
            return null
        }
    })
}

const getSteamInfoFromAPI = function (steamID) {
    let steamInfo = {
        steamId: steamID
    }
    //I decided to parse information from steamdb.info not from steamAPI because
    //steamAPI allows to connect only 200 times in 5 minutes where from steamDB
    //I can parse 4-5 more in the same time
    return request({
        headers: { 'User-Agent': support.getRandomUserAgent() },
        url: steamDBURL + "/app/" + steamID + "/info/",
        json: false
    }).then(function (body) {
        const $ = cheerio.load(body)
        let html = "<table>" + $('div#info').find('table').html() + "</table>"
        var tablesAsJson = tabletojson.convert(html)[0]
        support.returnValueOfTheKeyFromArrayOfObjects(tablesAsJson, "Genres").then(genres => {
            steamInfo.genres = genres
        })
        steamGameParsed = steamGameParsed + 1
        //steamDB.info give IP ban if someone parse more than 1200 times from their server
        //so thats why after 1000 records there is a break for 3 minutes
        if(steamGameParsed >= 1000){
            sleep(180000)
            steamGameParsed = 0
        }
        return steamInfo
    }).catch(error => {
        console.log(new Error(error))
    })
}

const getSteamIDbyTitle = function (propertyValue) {
    return support.findObjectPropertyInArray(steamAppsArr, "name", propertyValue).then(obj => {
        if (obj !== undefined) {
            return obj.appid
        } else {
            return getSteamIDbyTitleSteamDB(propertyValue).then(steamID => {
                if (steamID == null && support.isStringContains(propertyValue, "'")) {
                    return getSteamIDbyTitle(propertyValue.replace("'", '’'))
                } else {
                    return steamID
                }
            })
        }
    })
}

const getSteamIDbyTitleSteamDB = function (name) {
    return request({
        headers: { 'User-Agent': support.getRandomUserAgent() },
        url: steamDBURL + "/search/?q=" + name,
        json: false
    })
        .then(function (body) {
            const $ = cheerio.load(body)
            let steamID = $('tr.app').attr("data-appid")
            return (steamID !== undefined) ? support.parseInt(steamID) : null
        }).catch(error => {
            console.log(new Error(error))
        })
}

exports.getCapsuleFromSteam = function (steamID) {
    return (steamID !== null && steamID !== undefined) ? "http://cdn.akamai.steamstatic.com/steam/apps/" + steamID + "/capsule_616x353.jpg" : null
}