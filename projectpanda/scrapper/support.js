const request = require('request-promise'),
    dateTime = require('node-datetime'),
    cookie = require('tough-cookie'),
    imagesize = require('imagesize'),
    random_ua = require('random-ua'),
    download = require('download'),
    cheerio = require('cheerio'),
    pathModule = require('path'),
    moment = require('moment'),
    https = require('https'),
    fs = require('fs')

exports.isStringContains = function (string, search) {
    return string.search(search) !== -1
}

exports.elemExist = function (dom) {
    return dom.length > 0
}

exports.dynamicSort = function (property) {
    var sortOrder = 1
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0
        return result * sortOrder
    }
}

exports.parseInt = function (string) {
    return (string !== null && string !== undefined) ? parseInt(string) : string
}

exports.findObjectPropertyInArray = function (array, propertyName, propertyValue) {
    return Promise.resolve(array.filter(function (obj) {
        return obj[propertyName] === propertyValue
    })[0])
}

exports.returnObjectByPropertyInArray = function (array, propertyName, propertyValue) {
    return Promise.resolve(array.find(x => x[propertyName] === propertyValue))
}

exports.returnValueOfTheKeyFromArrayOfObjects = function (array, propertyName) {
    return new Promise(function (resolve) {
        array.find(function (obj) {
            if (propertyName in obj) {
                resolve(obj[propertyName])
            }
        })
    })
}

exports.getArrayOfElementsFromArrayOfObjects = function (array, propertyName) {
    arrayOfElements = []
    array.forEach(function (elem) {
        arrayOfElements.push(elem[propertyName])
    })
    return arrayOfElements
}

exports.downloadFileFromURL = function (url, path, filename) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path)
    }
    if (url !== null && url !== undefined) {
        return request(url).then(() => {
            download(url).pipe(fs.createWriteStream(path + filename))
            return filename
        }).catch(error => {
            if (error.statusCode !== 404) {
                console.log(new Error(error))
                console.log("Error occured with ", url)
            }
            return null
        })
    } else {
        return Promise.resolve(null)
    }
}

exports.getTimestampFromDate = function (date) {
    return (date) ? Date.parse(removeDateOrdinals(date)) : null
}

const removeDateOrdinals = function (date) {
    return date.replace(/(\d+)(st|nd|rd|th)/, "$1")
}

exports.getDateFromTimestamp = function (timestamp, format) {
    return moment(timestamp).format(format)
}

exports.createCurrentDateByFormat = function (format) {
    return dateTime.create().format(format)
}

exports.getCurrentDateTimestamp = function () {
    return Date.now()
}

exports.getCurrentMoment = function () {
    return moment()
}

exports.getMomentElapsedTime = function (start) {
    var end = moment()
    var duration = moment.duration(end.diff(start))
    return console.log("Elapsed: " + duration.hours() + "h:" + duration.minutes() + "m:" + duration.seconds() + "s")
}

exports.getProjectPath = function () {
    return pathModule.join(__dirname, '../')
}

exports.getFirstElementsOfArray = function (array, size) {
    return array.slice(0, size)
}

exports.getMetacriticScore = function (gameTitle, console) {
    if (console == null) console = ""

    return request({
        url: "http://www.metacritic.com/game/" + console.replace(/ /g, '-').toLowerCase() + "/"
        + gameTitle.replace(/ /g, '-').toLowerCase() + "/critic-reviews",
        headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36'
        }
    })
        .then(function (body) {
            const dom = cheerio.load(body)
            let rating = dom('span[itemprop="ratingValue"]').text()
            return (rating) ? rating : null
        }).catch(error => {
            //console.log(new Error(error))
        })
}

exports.returnGamingConsole = function (console, client) {
    return (console !== "PC") ? client : console
}

exports.isImageLandscapeMode = function (url) {
    return Promise.resolve(https.get(url, function (response) {
        return imagesize(response, function (err, result) {
            return (result.width > result.height) ? true : false
        })
    }))
}

exports.getRandomUserAgent = function () {
    return random_ua.generate()
}

exports.setCookieForUrl = function (cookieParam, mainDomain) {
    let newCookie = new cookie.Cookie(cookieParam)

    var cookiejar = request.jar()
    cookiejar.setCookie(newCookie, mainDomain)

    return cookiejar
}