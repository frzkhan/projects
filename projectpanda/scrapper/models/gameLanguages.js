
module.exports = (sequelize, Sequelize) => {
    return sequelize.define('p_game_languages', {
        languageName: {
          type: Sequelize.STRING
        },
        languageHash: {
          type: Sequelize.STRING
        }
      })
  }