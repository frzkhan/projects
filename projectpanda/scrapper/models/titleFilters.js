
module.exports = (sequelize, Sequelize) => {
    return sequelize.define('p_title_parsers', {
        filterName: {
          type: Sequelize.STRING
        },
        regex: {
          type: Sequelize.TEXT
        }
      })
  }