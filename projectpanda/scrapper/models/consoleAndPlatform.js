
module.exports = (sequelize, Sequelize) => {
    return sequelize.define('p_consoles_platforms', {
        console: {
          type: Sequelize.STRING
        },
        platform: {
          type: Sequelize.STRING
        },
        name: {
          type: Sequelize.STRING
        }
      })
  }