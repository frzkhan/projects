
module.exports = (sequelize, Sequelize) => {
    return sequelize.define('p_games_info', {
        game_id: {
          type: Sequelize.INTEGER,
          unique: true
        },
        title: {
          type: Sequelize.TEXT,
          unique: true,
          allowNull: false
        },
        cover: {
          type: Sequelize.TEXT
        },
        header: {
          type: Sequelize.TEXT
        },
        minPrice: {
          type: Sequelize.JSON
        },
        nextPrice: {
          type: Sequelize.JSON
        },
        console: {
          type: Sequelize.INTEGER
        },
        dlc: {
          type: Sequelize.BOOLEAN
        },
        languages: {
          type: Sequelize.TEXT
        },
        rating: {
          type: Sequelize.INTEGER
        },
        releaseDate: {
          type: Sequelize.DATE
        },
        genre: {
          type: Sequelize.TEXT
        },
        description: {
          type: Sequelize.TEXT
        },
        gallery: {
          type: Sequelize.TEXT
        },
        requirements: {
          type: Sequelize.TEXT
        },
        clicks: {
          type: Sequelize.INTEGER
        },
        approved: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        }
      })
  }