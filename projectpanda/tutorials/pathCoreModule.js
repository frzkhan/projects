var path = require("path")
var websiteHome = "Desktop/Bucky//thenewboston/index.html"
var websiteAbout = "Desktop/Bucky/thenewboston/about.html"

//użyteczna funkcja która pozwala poprawiać możliwie błędne ścieżki dostepu do serwera
console.log(path.normalize(websiteHome))
//można z path wyciągnać jaki jest to folder, jaka nazwa pliku oraz jakie jest jego
//rozszerzenie
console.log(path.dirname(websiteAbout))
console.log(path.basename(websiteAbout))
console.log(path.extname(websiteAbout))