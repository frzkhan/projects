//funkcja bez niczego zawsze zwraca undefined
function worthless(){
}

console.log(worthless())

//funkcja bez parametrow moze byc uzywana po nazwie jako zmienna
var printBacon = function () {
    console.log("lubie bekon")
}

setTimeout(printBacon, 2000) // wyświetla kod po jakimś czasie tylko raz

//uzywanie callbackow
function placeOrder(orderNumber){
    console.log("Customer order: ", orderNumber)

    cookAndDeliverFood(function(){
        console.log("Delivered food order: ", orderNumber)
    })
}

function cookAndDeliverFood(callback){
    setTimeout(callback, 5000)
}

placeOrder(1)
placeOrder(2)
placeOrder(3)
placeOrder(4)
placeOrder(5)
placeOrder(6)

//wszystko jest referencja w Node.js
var Bucky = {
    favFood: "Bacon",
    favMovie: "Chappie"
}

var Person = Bucky
Person.favFood = "Salat"
console.log(Bucky.favFood)

//roznica miedzy == a ===
console.log(19 == "19") //true
console.log(19 === "19") //false

//idea this
var Bucky = {
    printFirstName: function(){
        console.log("My name is Bucky")
        console.log("czy to Bucky?:", this === Bucky)
    }
}

Bucky.printFirstName()

function doSomethingWorthless() {
    console.log("My name is worthless")
    console.log("czy to Bucky?:", this === Bucky)
    console.log("czy to global?:", this === global)
}

doSomethingWorthless()

//setInterval pozwala uruchomić kod raz i za każdym ustawionym interwałem
//wykonywać wewnętrzny kod

setInterval(function(){
    console.log("lubię placki")
}, 2000)

//__dirname zwraca ścieżkę w której jest uruchamiany kod
console.log(__dirname)
//__filename zwraca ścieżkę w której jest uruchamiany kod wraz z nazwą kodu i
//jego rozszerzeniem
console.log(__filename)