const requestPromise = require('request-promise')
const requestPromiseRetry = require( 'request-promise-retry' );
const request = requestPromiseRetry( requestPromise );
const cheerio = require('cheerio')
const P = require('bluebird')
const fs = require('fs')
const webUrl = "https://www.size.co.uk/mens/footwear/sale/?AJAX=1"

let totalPages = null
let products = []

function sendRequest(pageNumber, delay) {
  return P.delay(delay)
  .then(() => {
    let url = webUrl

    if(pageNumber) {
      url = `${webUrl}&from=${pageNumber}`
    }
    return request(url)
    .then(data => {
      const $ = cheerio.load(data)
      $('.productListItem').each((i, p) => {
        const product = {
          url: "https://www.size.co.uk"+$(p).find('.itemTitle a').attr('href'),
          title: $(p).find('.itemTitle a').html(),
          image: $(p).find('.itemImage img').attr('src'),
          oldPrice: $(p).find('.itemPrice .was span').html(),
          newPrice: $(p).find('.itemPrice .now span').html(),
        }
        products.push(product)
      })

      console.log(`Products parsed: ${products.length}`);
      totalPages = Math.ceil($('.pageCount').html().replace(/\D+/g, '')/24)
    })
  })
}
function getProductPage (product, delay) {
  return P.delay(delay)
  .then(() => request(product.url))
  .then(data => {
    console.log(`getting size for product: ${product.title}`);
    const $ = cheerio.load(data)
    product.sizes = []
    $('.options button').each((i, button) => {
      product.sizes.push($(button).html().trim())
    })
    return product
  })
  .catch(error => {
    console.log(error);
  })
}
sendRequest()
.then(() => {

  const promises = []
  for(var i = 1; i < totalPages; i ++) {
    const delay = i * 1000
    promises.push(sendRequest(24 * i, delay))
  }
  return P.all(promises)
})
.then(() => {
  console.log(`Total Products: ${products.length}`);
  console.log('Getting sizes');
  let delay = 0
  return P.each(products, product => {
    return getProductPage(product, 1)
  })
  return P.all(products.map(product => {
    delay += 800
    return getProductPage(product, delay)
  }))
})
.then(productsList => {
  fs.writeFile('products.json', JSON.stringify(productsList, null, ' '), (err) => {
    if(err) console.log(err);
    console.log('DONE!!!');
  })
})
.catch(error => {
  console.error(error);
})
