var https = require('https');
var fs = require('fs');
var options = {
  rejectUnauthorized: false,
  hostname: 'servername.com',
  port: 4242,
  path: '/qrs/app?xrfkey=abcdefghijklmnop',
  method: 'GET',
  headers: {
    'x-qlik-xrfkey' : 'abcdefghijklmnop',
    'X-Qlik-User' : 'UserDirectory= Internal; UserId= sa_repository ' 
  },
  key: fs.readFileSync("C:\\client.pem"),
  cert: fs.readFileSync("C:\\client.pem")
};
https.get(options, function(res) {
  console.log("Got response: " + res.statusCode);
  res.on("data", function(chunk) {
    console.log("BODY: " + chunk);
  });
})
.on('error', function(e) {
  console.log("Got error: " + e.message);
});
