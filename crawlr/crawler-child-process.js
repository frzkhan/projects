var fs = require('fs');
var cp = require('child_process');
var async = require('async');

var requestData = './request-data'

fs.readFile('urls', 'utf8', (err, data) => {
  if (err) throw err;
  var urls = data.split('\n');
  var processedUrls = 0
  var totalUrls = urls.length

  async.eachLimit(
    urls,
    20,
    function(url, callback){
      var child = cp.fork(requestData)
      child.on('message', function (status) {
        console.log(status);
        processedUrls ++
        if(processedUrls === totalUrls) {
          process.exit()
        }
        callback()
        child.kill()
      })
      child.send(url)
    },
    function(err){
       console.log(err, 'async error');
    }
  );
});
