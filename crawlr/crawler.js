var request = require('request-promise');
var app = require('express')();
var fs      = require('fs');
var cheerio = require('cheerio');

app.get('/scrape', function(req, res) {
  var url = req.query.url;
  return request(url)
  .then(function (html) {
    var json = parseData(html, url)
    save('success', url)
    var jsonData = JSON.stringify(json, null, '\t');
    save('successData', jsonData)
    res.send(jsonData)
    return
  })
  .catch(function (error) {
    save('failed', url)
    console.log(error);
    res.end()
    return
  })
})
app.listen('8081')
console.log('Server running at port 8081');

function parseData (html, url) {
  var $ = cheerio.load(html);

  var json = {};

  var mtitle = $('meta[name="title"]').attr('content');
  var mkeywords = $('meta[name="keywords"]').attr('content');
  var mdescription = $('meta[name="description"]').attr('content');

  var otype = $('meta[property="og:type"]').attr('content');
  var otitle = $('meta[property="og:title"]').attr('content');
  var ovideourl = $('meta[property="og:video:url"]').attr('content');
  var oimage = $('meta[property="og:image"]').attr('content');
  var odescription = $('meta[property="og:description"]').attr('content');
  var osite = $('meta[property="og:site_name"]').attr('content');
  var odate = $('meta[property="article:published_time"]').attr('content');


  var tsite = $('meta[name="twitter:site"]').attr('content');
  var turl = $('meta[name="twitter:url"]').attr('content');
  var ttitle = $('meta[name="twitter:title"]').attr('content');
  var tdescription = $('meta[name="twitter:description"]').attr('content');
  var timage = $('meta[name="twitter:image"]').attr('content');
  var tplayer = $('meta[name="twitter:player"]').attr('content');

  var psite = $('meta[property="twitter:site"]').attr('content');
  var purl = $('meta[property="twitter:url"]').attr('content');
  var ptitle = $('meta[property="twitter:title"]').attr('content');
  var pdescription = $('meta[property="twitter:description"]').attr('content');
  var pimage = $('meta[property="twitter:image"]').attr('content');
  var pplayer = $('meta[property="twitter:player"]').attr('content');

  if (osite)
  {json.site=osite;}
  else if (tsite)
  {json.site=tsite;}
  else if (psite)
  {json.site=psite;}

  json.url=url;

  if (ttitle)
  {json.title=ttitle;}
  else if (ptitle)
  {json.title=ptitle;}
  else if (otitle)
  {json.title=otitle;}
  else if (mtitle)
  {json.title=mtitle;}

  json.keywords = mkeywords;

  if (tdescription)
  {json.description=tdescription;}
  else if (pdescription)
  {json.description=pdescription;}
  else if (odescription)
  {json.description=odescription;}
  else if (mdescription)
  {json.description=mdescription;}


  if (timage)
  {json.image=timage;}
  else if (pimage)
  {json.image=pimage;}
  else if (oimage)
  {json.image=oimage;}


  if (tplayer)
  {json.player=tplayer;}
  else if (pplayer)
  {json.player=pplayer;}

  json.date = odate;


  var oembed = $('link[type="application/json+oembed"]').attr('href');


  json.oembed = oembed;
  return json
}
function save(file, text) {
  fs.appendFileSync(file, text + '\n');
}
