var express = require('express');
var fs      = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();

app.get('/scrape', function(req, res){
  //res.setHeader('Access-Control-Allow-Origin', '*');

  var url = req.query.url;
  request(url, {jar: true}, function(error, response, html){
    if(error) {
      fs.appendFile('failed.txt', url + '\r\n', function(err) {
        if(err) {
          console.error('Write failed: ', err);
        }
      });
      res.end()
      return
    }
    if(!error){
      var $ = cheerio.load(html);

      var title, release, rating;
      var json = { title : "", site : "", description : "", url : "", player : "", image : ""};

      var mtitle = $('meta[name="title"]').attr('content');
      var mkeywords = $('meta[name="keywords"]').attr('content');
      var mdescription = $('meta[name="description"]').attr('content');

      var otype = $('meta[property="og:type"]').attr('content');
      var otitle = $('meta[property="og:title"]').attr('content');
      var ovideourl = $('meta[property="og:video:url"]').attr('content');
      var oimage = $('meta[property="og:image"]').attr('content');
      var odescription = $('meta[property="og:description"]').attr('content');

      var tsite = $('meta[name="twitter:site"]').attr('content');
      var turl = $('meta[name="twitter:url"]').attr('content');
      var ttitle = $('meta[name="twitter:title"]').attr('content');
      var tdescription = $('meta[name="twitter:description"]').attr('content');
      var timage = $('meta[name="twitter:image"]').attr('content');
      var tplayer = $('meta[name="twitter:player"]').attr('content');

      var psite = $('meta[property="twitter:site"]').attr('content');
      var purl = $('meta[property="twitter:url"]').attr('content');
      var ptitle = $('meta[property="twitter:title"]').attr('content');
      var pdescription = $('meta[property="twitter:description"]').attr('content');
      var pimage = $('meta[property="twitter:image"]').attr('content');
      var pplayer = $('meta[property="twitter:player"]').attr('content');

      if (tsite)
      {json.site=tsite;}
      else if (psite)
      {json.site=psite;}

      if (turl)
      {json.url=turl;}
      else if (purl)
      {json.url=purl;}

      if (ttitle)
      {json.title=ttitle;}
      else if (ptitle)
      {json.title=ptitle;}
      else if (otitle)
      {json.title=otitle;}
      else if (mtitle)
      {json.title=mtitle;}

      if (tdescription)
      {json.description=tdescription;}
      else if (pdescription)
      {json.description=pdescription;}
      else if (odescription)
      {json.description=odescription;}
      else if (mdescription)
      {json.description=mdescription;}


      if (timage)
      {json.image=timage;}
      else if (pimage)
      {json.image=pimage;}
      else if (oimage)
      {json.image=oimage;}


      if (tplayer)
      {json.player=tplayer;}
      else if (pplayer)
      {json.player=pplayer;}



      var oembed = $('link[type="application/json+oembed"]').attr('href');


      json.oembed = oembed;
    }

    //    fs.writeFile('output.json', JSON.stringify(json, null, 4), function(err){
    //    console.log('File successfully written! - Check your project directory for the output.json file');
    //    })

    res.send( JSON.stringify(json, null, '\t'))
  })
})

app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;
